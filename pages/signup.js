import { Form, Input, Button, Checkbox } from "antd";
import axios from 'axios';



import React from "react";
class SignUp extends React.Component
{
  constructor(props)
  {
    super(props);
    this.state = {username: '',password:''};
    this.handleUserNameChange = this.handleUserNameChange.bind(this);
    this.handlePasswordNameChange = this.handlePasswordNameChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    

  }
  handleUserNameChange(event) {
    this.setState({username: event.target.value});
  }
  handlePasswordNameChange(event) {
    this.setState({password: event.target.value});
  }
  handleSubmit(event) {
    //alert('AA name was submitted: ' + this.state.username);
    const user = this.state;
    axios.post('/api/users', { user })
      .then(res => {
        if(res.data.status == 'success')
        {
          alert('login success');
        }
        console.log(res);
        console.log(res.data);
      })
    event.preventDefault();
  }
  render() {
    const layout = {
      labelCol: {
        span: 4,
      },
      wrapperCol: {
        span: 8,
      },
    };
    const tailLayout = {
      wrapperCol: {
        offset: 4,
        span: 8,
      },
    };
    return (
      <div>                
      
      <Form
        {...layout}
        name="basic"
        initialValues={{
          remember: true,
        }}
        //onFinish={onFinish}
        //onFinishFailed={onFinishFailed}
      >
        <Form.Item
          label="Username"
          name="username"
          rules={[
            {
              required: true,
              message: "Please input your username!",
            },
          ]}
          onChange={this.handleUserNameChange} 
        >
          <Input />
        </Form.Item>
  
        <Form.Item
          label="Password"
          name="password"
          rules={[
            {
              required: true,
              message: "Please input your password!",
            },
          ]}
          onChange={this.handlePasswordNameChange} 
        >
          <Input.Password />
        </Form.Item>
  
        <Form.Item {...tailLayout} name="remember" valuePropName="checked">
          <Checkbox>Remember me</Checkbox>
        </Form.Item>
  
        <Form.Item {...tailLayout}>
          <Button type="primary" htmlType="submit" onClick={this.handleSubmit}>
            Submit
          </Button>
        </Form.Item>
      </Form>
      </div>
    );
  }
}
export default SignUp
